using UnityEngine;

namespace Utilities
{
    // <summary>
    // Inherit from this base class to create a singleton.
    // e.g. public class MyClassName : Singleton<MyClassName> {}
    // </summary>
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        // Check to see if we're about to be destroyed.
        private static bool ShuttingDown = false;
        private static object Lock = new object();
        private static T mInstance;
 
        // <summary>
        // Access singleton instance through this propriety.
        // </summary>
        public static T Instance
        {
            get
            {
                if (ShuttingDown)
                {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                                     "' already destroyed. Returning null.");
                    return null;
                }
 
                lock (Lock)
                {
                    if (mInstance == null)
                    {
                        // Search for existing instance.
                        mInstance = (T)FindObjectOfType(typeof(T));
 
                        // Create new instance if one doesn't already exist.
                        if (mInstance == null)
                        {
                            // Need to create a new GameObject to attach the singleton to.
                            var singletonObject = new GameObject();
                            mInstance = singletonObject.AddComponent<T>();
                            singletonObject.name = typeof(T).ToString() + " (Singleton)";
 
                            // Make instance persistent.
                            DontDestroyOnLoad(singletonObject);
                        }
                    }
 
                    return mInstance;
                }
            }
        }
 
 
        private void OnApplicationQuit()
        {
            ShuttingDown = true;
        }
 
 
        private void OnDestroy()
        {
            ShuttingDown = true;
        }
    }
}
