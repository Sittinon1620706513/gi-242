﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class ResultDialog : MonoBehaviour
    {
        [SerializeField] private Button restartButton;
        [SerializeField] private TextMeshProUGUI highScoreText;

        public void Init()
        {
            SetHighScoreText(ScoreManager.Instance.score);
        }


        private void Awake()
        {
            Debug.Assert(restartButton != null, "restartButton != null");
            Debug.Assert(highScoreText != null, "highScoreText != null");
            
            restartButton.onClick.AddListener(OnRestartButtonClicked);
        }

        private void SetHighScoreText(int score)
        {
            var text = highScoreText.text;
            var s = $"High score : {score}";
        }

        private void OnRestartButtonClicked()
        {
            gameObject.SetActive(false);
            GameManager.Instance.StartGame();
        }
    }
}