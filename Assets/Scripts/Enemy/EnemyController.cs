﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship _enemySSpaceship;
        [SerializeField] private float chasingThresholdDistance;
        private void Update()
        {
            MoveToPlayer();
            _enemySSpaceship.Fire();
        }

         private void MoveToPlayer()
         {
             // TODO: Implement this later
         }
    }    
}

