using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace  EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private Transform playerTransform;
        [SerializeField] private float enemyShipSpeed;
        private float minDistanceToPlayer = 1.0f;
  

        // Update is called once per frame
        void Update()
        {
            Vector2 displacementToplayer = playerTransform.position - transform.position;
            Vector2 directtionToPlayer = displacementToplayer.normalized;
            float distanceToPlayer = displacementToplayer.magnitude;

            Vector2 enemyVelocity = displacementToplayer * enemyShipSpeed;

            if (distanceToPlayer > minDistanceToPlayer)
            {
                transform.Translate(enemyVelocity * Time.deltaTime);
            }
         
        }
    }

}

